package utils;

import model.Apartment;
import model.Bill;
import model.Customer;
import model.Employee;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;


public class HibernateUtil {
	private static ServiceRegistry serviceRegistry;
	private static SessionFactory sessionFactory;

	public static SessionFactory getSessionFactory() {
		/* Cách 1*/
//		if (sessionFactory == null) {
//			Configuration configuration = new Configuration();
//			Properties properties = new Properties();
//			properties.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
//			properties.put(Environment.URL, "jdbc:mysql://localhost:3306/hibernate_exam_final?useSSL=false");
//			properties.put(Environment.USER, "root");
//			properties.put(Environment.PASS, "1234");
//			properties.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
//			properties.put(Environment.SHOW_SQL, "true");
//			properties.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
//			properties.put(Environment.HBM2DDL_AUTO, "create-drop");
//
//			configuration.setProperties(properties);
//			configuration.addAnnotatedClass(Employee.class);
//			configuration.addAnnotatedClass(Customer.class);
//			configuration.addAnnotatedClass(Apartment.class);
//			configuration.addAnnotatedClass(Bill.class);
//
//			serviceRegistry = new StandardServiceRegistryBuilder()
//					.applySettings(configuration.getProperties()).build();
//
//			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
//		}
//		return sessionFactory;

		/* Cach 2 */
		if (sessionFactory == null) {
			Configuration configuration = new Configuration();
			sessionFactory = configuration.configure().buildSessionFactory();
		}
		return sessionFactory;
	}

	public static void closeConnection() {
		if (serviceRegistry != null) {
			StandardServiceRegistryBuilder.destroy(serviceRegistry);
		}
	}
}
