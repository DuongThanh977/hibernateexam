package model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column
	private String customerName;

	@Column
	private String phone;

	@Column
	private LocalDate birthDate;

	@Column
	private String address;

	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
	private Set<Apartment> apartments = new HashSet<>();

	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
	private Set<Bill> bills = new HashSet<>();

	@ManyToMany(mappedBy = "customers", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Employee> employee = new HashSet<>();

	public Customer() {
	}

	public Customer(String customerName, String phone, LocalDate birthDate, String address) {
		this.customerName = customerName;
		this.phone = phone;
		this.birthDate = birthDate;
		this.address = address;
	}

	public Customer(String customerName, String phone, LocalDate birthDate, String address, Set<Apartment> apartments, Set<Bill> bills, Set<Employee> employee) {
		this.customerName = customerName;
		this.phone = phone;
		this.birthDate = birthDate;
		this.address = address;
		this.apartments = apartments;
		this.bills = bills;
		this.employee = employee;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Set<Apartment> getApartments() {
		return apartments;
	}

	public void setApartments(Set<Apartment> apartments) {
		this.apartments = apartments;
	}

	public Set<Bill> getBills() {
		return bills;
	}

	public void setBills(Set<Bill> bills) {
		this.bills = bills;
	}

	public Set<Employee> getEmployee() {
		return employee;
	}

	public void setEmployee(Set<Employee> employee) {
		this.employee = employee;
	}

	@Override
	public String toString() {
		return "Customer{" +
				"id=" + id +
				", customerName='" + customerName + '\'' +
				", phone='" + phone + '\'' +
				", birthDate=" + birthDate +
				", address='" + address + '\'' +
				'}';
	}
}
