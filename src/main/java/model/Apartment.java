package model;

import javax.persistence.*;

@Entity
public class Apartment {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column
	private String apartmentCode;

	@Column
	private Integer numBedRoom;

	@Column
	private String doorDirection;

	@Column
	private Long price;

	@Column
	private String status;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "employee_id")
	private Employee employee;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "customer_id")
	private Customer customer;

	public Apartment() {
	}

	public Apartment(String apartmentCode, Integer numBedRoom, String doorDirection, Long price, String status, Employee employee, Customer customer) {
		this.apartmentCode = apartmentCode;
		this.numBedRoom = numBedRoom;
		this.doorDirection = doorDirection;
		this.price = price;
		this.status = status;
		this.employee = employee;
		this.customer = customer;
	}

	public Apartment(String apartmentCode, Integer numBedRoom, String doorDirection, Long price, String status) {
		this.apartmentCode = apartmentCode;
		this.numBedRoom = numBedRoom;
		this.doorDirection = doorDirection;
		this.price = price;
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApartmentCode() {
		return apartmentCode;
	}

	public void setApartmentCode(String apartmentCode) {
		this.apartmentCode = apartmentCode;
	}

	public Integer getNumBedroom() {
		return numBedRoom;
	}

	public void setNumBedroom(Integer numBedRoom) {
		this.numBedRoom = numBedRoom;
	}

	public String getDoorDirection() {
		return doorDirection;
	}

	public void setDoorDirection(String doorDirection) {
		this.doorDirection = doorDirection;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "Apartment{" +
				"id=" + id +
				", apartmentCode='" + apartmentCode + '\'' +
				", numBedRoom=" + numBedRoom +
				", doorDirection='" + doorDirection + '\'' +
				", price=" + price +
				", status='" + status +
				'}';
	}
}
