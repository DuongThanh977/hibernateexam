package model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column
	private String employeeName;

	@Column
	private String roleName;

	@OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
	private Set<Apartment> apartments = new HashSet<>();

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "employee_customer",
			joinColumns = {@JoinColumn(name = "employee_id")},
			inverseJoinColumns = {@JoinColumn(name = "customer_id")} )
	private Set<Customer> customers = new HashSet<>();

	@OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
	private Set<Bill> bills = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Set<Apartment> getApartments() {
		return apartments;
	}

	public void setApartments(Set<Apartment> apartments) {
		this.apartments = apartments;
	}

	public Set<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}

	public Set<Bill> getBills() {
		return bills;
	}

	public void setBills(Set<Bill> bills) {
		this.bills = bills;
	}

	public Employee() {
	}

	public Employee(String employeeName, String roleName, Set<Apartment> apartments, Set<Customer> customers, Set<Bill> bills) {
		this.employeeName = employeeName;
		this.roleName = roleName;
		this.apartments = apartments;
		this.customers = customers;
		this.bills = bills;
	}

	public Employee(String employeeName, String roleName) {
		this.employeeName = employeeName;
		this.roleName = roleName;
	}

	@Override
	public String toString() {
		return "Employee{" +
				"id=" + id +
				", employeeName='" + employeeName + '\'' +
				", roleName='" + roleName + '\'' +
				'}';
	}
}
