import model.Apartment;
import model.Bill;
import model.Customer;
import model.Employee;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.HibernateUtil;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Dao {
	private Session session = null;
	public <S> void save(Session session, Iterable<S> entities) {
		for (S entity : entities) {
			session.save(entity);
		}
	}

	public void saveBill(Session session, List<Bill> entities) {
		for (Bill entity : entities) {
			entity.getCustomer().getEmployee().add(entity.getEmployee());
			entity.getEmployee().getCustomers().add(entity.getCustomer());
			session.save(entity);
			session.save(entity.getEmployee());
		}
	}

	public void start() {
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();

			List<Employee> employees = new ArrayList<>();
			employees.add(new Employee("Nguyễn Văn An", "QL"));
			employees.add(new Employee("Lý Văn Binh", "NV"));
			employees.add(new Employee("Nguyễn Mai Anh", "NV"));
			employees.add(new Employee("Trần Trọng Phúc", "NV"));
			save(session, employees);

			List<Customer> customers = new ArrayList<>();
			customers.add(new Customer("Nguyễn Huệ", "0380113115", LocalDate.of(1945, 10, 10), "Bình Định"));
			customers.add(new Customer("Trần Hưng Đạo", "0380114116", LocalDate.of(1954, 12, 11), "Hà Nội"));
			customers.add(new Customer("Gia Long", "0380112115", LocalDate.of(1975, 04, 30), "Thừa Thiên Huế"));
			customers.add(new Customer("Hồ Quý Ly", "0380113116", LocalDate.of(1989, 12, 02), "Quảng Trị"));
			customers.add(new Customer("Lý Thường Kiệt", "0380114115", LocalDate.of(1990, 10, 15), "Hà Nội"));
			save(session, customers);

			List<Apartment> apartments = new ArrayList<>();
			apartments.add(new Apartment("CH-1001", 1, "Đông", 1500000000L, "đã bán", employees.get(1), customers.get(0)));
			apartments.add(new Apartment("CH-1002", 1, "Bắc", 1600000000L, "đã bán", employees.get(2), customers.get(4)));
			apartments.add(new Apartment("CH-1003", 1, "Tây", 1500000000L, "chưa bán"));
			apartments.add(new Apartment("CH-1004", 2, "Nam", 1900000000L, "đã bán", employees.get(3), customers.get(1)));
			apartments.add(new Apartment("CH-1005", 2, "Đông", 1500000000L, "chưa bán"));
			apartments.add(new Apartment("CH-1006", 2, "Bắc", 1400000000L, "chưa bán"));
			apartments.add(new Apartment("CH-1007", 2, "Tây", 1500000000L, "đã bán", employees.get(1), customers.get(3)));
			apartments.add(new Apartment("CH-1008", 2, "Nam", 1900000000L, "chưa bán"));
			apartments.add(new Apartment("CH-1009", 3, "Nam", 1900000000L, "đã bán", employees.get(1), customers.get(3)));
			apartments.add(new Apartment("CH-1010", 3, "Đông", 1500000000L, "đã bán", employees.get(3), customers.get(2)));
			save(session, apartments);

			List<Bill> bills = new ArrayList<>();
			bills.add(new Bill(employees.get(1), apartments.get(0), customers.get(0), LocalDate.of(2020,01, 01), apartments.get(0).getPrice()));
			bills.add(new Bill(employees.get(1), apartments.get(6), customers.get(3), LocalDate.of(2020,01, 01), apartments.get(6).getPrice()));
			bills.add(new Bill(employees.get(1), apartments.get(8), customers.get(3), LocalDate.of(2020,01, 01), apartments.get(8).getPrice()));
			bills.add(new Bill(employees.get(2), apartments.get(1), customers.get(4), LocalDate.of(2020,01, 01), apartments.get(1).getPrice()));
			bills.add(new Bill(employees.get(3), apartments.get(3), customers.get(1), LocalDate.of(2020,01, 01), apartments.get(3).getPrice()));
			bills.add(new Bill(employees.get(3), apartments.get(9), customers.get(2), LocalDate.of(2020,01, 01), apartments.get(9).getPrice()));
			saveBill(session, bills);

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
	}

	public List<Employee> getEmployees() {
		Transaction transaction = null;
		List<Employee> employees = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();

			Query query = session.createQuery("from Employee");
			employees = query.list();

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}

		return employees;
	}

	public List<Customer> getCustomers() {
		Transaction transaction = null;
		List<Customer> customers = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();

			Query query = session.createQuery("from Customer");
			customers = query.list();

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}

		return customers;
	}

	public List<Apartment> getApartment(int numberOfBedRoom) {
		Transaction transaction = null;
		List<Apartment> apartments = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();

			Query query = session.createQuery("from Apartment where numBedRoom = :numBedRoom " +
					"and status='chưa bán'");
			query.setParameter("numBedRoom", numberOfBedRoom);
			apartments = query.list();

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}

		return apartments;
	}

	public boolean checkEmployee(int employeeId) {
		Boolean result = null;
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();

			String sql = "select count(*)" +
					" from employee e inner join apartment a on e.id = a.employee_id" +
					" group by e.id, e.employeeName" +
					" having e.id= :employeeId and count(e.id) >= 3";
			Query query = session.createSQLQuery(sql);
			query.setParameter("employeeId", employeeId);
			result = query.uniqueResult() == null;

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}

		return result;
	}

	public boolean checkCustomer(int customerId) {
		Boolean result = null;
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();

			String sql = "select count(c.id) " +
					"from customer c inner join apartment a on c.id = a.customer_id " +
					"group by c.id, c.customerName " +
					"having c.id= :customerId and count(c.id) >= 2";
			Query query = session.createSQLQuery(sql);
			query.setParameter("customerId", customerId);
			result = query.uniqueResult() == null;

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
		return result;
	}

	public boolean createBill(Apartment apartmentChosen, Customer customerChosen, Employee employeeChosen) {
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();

			Bill bill = new Bill(employeeChosen, apartmentChosen, customerChosen, LocalDate.now(), apartmentChosen.getPrice());;
			customerChosen.getEmployee().add(employeeChosen);
			employeeChosen.getCustomers().add(customerChosen);

			session.save(bill);
			session.save(customerChosen);
			transaction.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
		return false;
	}

	public boolean updateApartment(Apartment apartmentChosen, Customer customerChosen, Employee employeeChosen) {
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();

			Query query = session.createQuery("from Apartment where id = :id");
			query.setParameter("id", apartmentChosen.getId());
			Apartment apartment = (Apartment) query.uniqueResult();
			apartment.setEmployee(employeeChosen);
			apartment.setCustomer(customerChosen);
			apartment.setStatus("đã bán");

			session.saveOrUpdate(apartment);
			transaction.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
		return false;
	}
}
