import model.Apartment;
import model.Customer;
import model.Employee;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Scanner;

public class Application {
	private Employee employeeChosen = null;
	private Customer customerChosen = null;
	private Apartment apartmentChosen = null;

	public boolean cau1(Dao dao) {
		int employeeId;
		int customerId;
		int apartmentId;
		int bedRoom;
		Scanner input = new Scanner(System.in);

		System.out.println("Nhập số lượng phòng ngủ của căn hộ: ");
		bedRoom = input.nextInt();
		input.nextLine();

		List<Apartment> apartments = dao.getApartment(bedRoom);
		if (apartments.isEmpty()) {
			System.out.println("Hien tai da het can ho phu hop voi khach hang");
			return false;
		} else {
			apartments.forEach(apartment -> System.out.println(apartment.toString()));
		}

		System.out.println("Chọn ID căn hộ: ");
		apartmentId = input.nextInt();
		input.nextLine();
		apartmentChosen = apartments
				.stream()
				.filter(apartment -> apartmentId == apartment.getId())
				.findAny()
				.orElseThrow(() -> new EntityNotFoundException("Căn hộ không tồn tại"));

		List<Employee> employees = dao.getEmployees();
		employees.forEach(employee -> System.out.println(employee.toString()));
		System.out.println("Chọn ID nhân viên muốn tư vấn: ");
		employeeId = input.nextInt();
		input.nextLine();

		if (!dao.checkEmployee(employeeId)) {
			System.out.println("Nhan vien da ban qua so can ho cho phep");
			return false;
		}
		employeeChosen = employees.stream().filter(employee -> employeeId == employee.getId()).findAny().get();

		List<Customer> customers = dao.getCustomers();
		customers.forEach(apartment -> System.out.println(apartment.toString()));
		System.out.println("Chọn ID khách hàng muốn mua nhà: ");
		customerId= input.nextInt();
		input.nextLine();

		if (!dao.checkCustomer(customerId)) {
			System.out.println("Da het so luong can ho uu dai cho khach hang");
			return false;
		}
		customerChosen = customers.stream().filter(customer -> customerId == customer.getId()).findAny().get();

		return true;
	}

	private boolean cau2(Dao dao) {
		return dao.createBill(apartmentChosen, customerChosen, employeeChosen);
	}

	private boolean cau3(Dao dao) {
		return dao.updateApartment(apartmentChosen, customerChosen, employeeChosen);
	}

	public static void main(String[] args) {
		Dao dao = new Dao();
		Application app = new Application();

		dao.start();
		app.cau1(dao);

		if (app.cau2(dao)) {
			System.out.println("Tạo bill thành công");
			if (app.cau3(dao)) {
				System.out.println("Update căn hộ thành công");
			} else {
				System.out.println("Update căn hộ thất bại");
			}
		} else {
			System.out.println("Tạo bill thất bại");
		}

	}


}
